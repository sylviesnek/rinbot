# RinBot

## Current Features
- Optional logging of chat events and notifications when messages are edited or deleted.
- Welcoming message sent when someone new joins the server.
- A warm 'good morning' message sent once a day in response to server members saying good morning.
- Commands:
    - `!rin paint me <colour>` takes one of the seven colours of the rainbow or pink or garrow, and applies a role with that name to the user (setup required).
    - `!rin fetch my picture` returns a link to the user's display picture.
    - `!rin flip a coin` flips a coin and outputs the result.
    - `!rin roll a <dice definition>` rolls a number of dice. Definitions follow the following pattern: `<number of dice to roll>d<sides of the dice>`.
    - `!rin greet` sends a greeting message.
    - `!rin make tea` sends a roleplay message of the bot making tea.
    - `!rin play <url>` plays either an online audio file, or a youtube video in the voice channel the user is in.
    - `!rin transform <@target || me>` prints out a specification for a random form, along with a roleplay starter message.
- Admin / Guild Manager Commands:
    - `!rin set prefix <prefix>` changes the prfix for commands from `!rin`.
    - `!rin toggle edit notifications` toggles whether to receive notifications whenever members edit or delete a message, defaults to off.
    - `!rin set notifications channel` sets the channel in which notifications for edits and deletes ought to be posted.
    - `!rin toggle event logs` toggles whether a log of messages, edits, and deletions ought to be kept, defaults to off.
    - `!rin set welcome channel` set the channel in which welcome messages are sent to new server members.
	- `!rin toggle transformation consent` - toggles whether Rin's roleplay messages for the tranform command will contain non-consent.

## Future Development
- Jest tests added covering the event handlers.
- Jest tests added covering more of the commands. This is currently on-hold due to being a PIA given existing bugs and limitations in jest-discordjs-mocks.
- Commands for hugging/petting up other server members.
- Added variation or alternative text for the various commands.
- Separate reply messages from logic.
- Prune the npm dependencies. I'm pretty sure there's several unused.
- Separate out methods used by 'paint me' and 'play' to make them testable.
- Automagically add the missing colour roles.
 
## Dependencies
- ffmpeg (Used for the `play` command, can be ignored so long as the command is never used).

## Installation
- Setup a new bot in the [Discord developer portal](https://discord.com/login?redirect_to=%2Fdevelopers%2Fapplications).
- Copy the auth token for your new bot into `config.json`.
- Set your desired activity (status) for the bot in `config.json`.
- Run `npm i` in the root directory of the project.
- Run `npm start` to start the bot.
- Invite Rin to your server using an invite url generated in the Discord developer portal.

## Optional Setup
- Creating a channel called `bot-notice` will result in notifications of users editing or deleting their messages being sent to said channel.
- Adding roles named after the colours of the rainbow, plus 'pink', will allow them to be added by the `paint me <colour>` command. ('red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'pink')
