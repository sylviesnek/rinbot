module.exports = (msg, settings) => {
    if (msg.member.hasPermission(`MANAGE_GUILD`) || msg.member.hasPermission(`ADMINISTRATOR`)) {
        let guildConfig = settings.get(msg.guild.id);
        const prefix = guildConfig.prefix;
        const commandString = " set welcome channel ";
        const newWelcomeChannelName = msg.content.substr(prefix.length + commandString.length).toLowerCase();
        const newWelcomeChannel = msg.member.guild.channels.cache.find(chan => chan.name == newWelcomeChannelName);
        if (newWelcomeChannel) {
            guildConfig.welcomeChannel = newWelcomeChannelName;
            settings.set(msg.guild.id, guildConfig);
            msg.reply(`"I have now set the welcome channel for new members to #${guildConfig.welcomeChannel}."`);
        }
        else {
            msg.reply(`*Rin grumbles to herself, pouting her lips somewhat* "I can't find a channel with the name '${newWelcomeChannelName}'.`)
        }
    }
    else {
        msg.reply(`*The robotic maid avoids eye contact as she shuffles her feet together.* "I-I'm sorry, but you don't have the authority t-to command me to do that..."`);
    }
}
