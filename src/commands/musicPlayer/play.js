module.exports = (msg, settings) => {
    const ytdl = require('ytdl-core');

    const prefix = settings.get(msg.guild.id).prefix;
    const playCommand = " play ";
    const audioUrl = msg.content.substr(prefix.length + playCommand.length);

    let playAudio = function() {
		const memberVoiceChannel = msg.member.voice.channel;

		if (memberVoiceChannel != null){
			memberVoiceChannel.join().then(() => {
				const currentConnection = msg.guild.me.voice.connection;

				if(currentConnection != null){
					const dispatcher = currentConnection.play(getAudio(audioUrl));
					dispatcher.on('start', () => {
						msg.reply(`*The robotic cat women's ears flatten for a moment, clicking several times `
							+ `before perking up again, the front of her ears seemingly now speakers.* "N-now playing your `
							+ `requested audio."`);
					});
					dispatcher.on('error', () => {
						msg.reply(`"S-sorry, for some reason I'm unable to play that audio..."`)
					})
				}
			}).catch(() => {
				msg.reply(`"Oh... I don't think I'm allowed in that audio channel..."`);
			});
		}
		else{
			msg.reply(`*Rin holds her head against her chest glumly* "P-please enter a voice channel before requesting `
			+ ` I play music..."`);
		}
    }

    let getAudio = function(source){
		if (source.includes("youtube.com") || source.includes("youtu.be")){
			return ytdl(source);
		}
		else{
			return source;
		}
    }

	playAudio();
}
