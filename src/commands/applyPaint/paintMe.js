module.exports = (msg, settings) => {
    const prefix = settings.get(msg.guild.id).prefix;
    const paintCommand = " paint me ";
    const colourArray = ["red","orange","yellow","green","blue","indigo","violet","pink"];
    let serverColourRoles = [];
    let userColourRoles = [];
    const paintColour = msg.content.substr(prefix.length + paintCommand.length).toLowerCase();

    colourArray.forEach((colourName, index) => {
		let role = msg.member.guild.roles.cache.find(role => role.name === colourName);

		if (!role) {
			console.log(`Missing role: ${colourName}`);
		}
		else{
			serverColourRoles.push(role);
			let userHasRole = msg.member.roles.cache.find(userRole => userRole.name === role.name)

			if (userHasRole) {
				userColourRoles.push(role);
			}
		}
	});

    let addRole = function(roleName) {
		let roleToAdd = findRole(roleName);
		if (roleToAdd) {
			msg.member.roles.add(roleToAdd);
		}
		else {
			msg.reply(`"I-I'm sorry... I don't seem to have ${roleName} paint..."`);
		}
	}

    let findRole = function(roleName) {
    	let foundRole = serverColourRoles.find(role => role.name === roleName);
    	 return foundRole;
	}

    let paintUser = async function() {
		switch(paintColour) {
			case "red":
				msg.member.roles.remove(userColourRoles).then(() => {
					addRole("red");
				}).catch((err) => {
					console.log(err);
				});
				break;

			case "orange":
				msg.member.roles.remove(userColourRoles).then(() => {
					addRole("orange");
				}).catch((err) => {
					console.log(err);
				});
				break;

			case "yellow":
				msg.member.roles.remove(userColourRoles).then(() => {
					addRole("yellow");
					return true;
				}).catch((err) => {
					console.log(err);
				});

				break;

			case "green":
				msg.member.roles.remove(userColourRoles).then(() => {
					addRole("green");
				}).catch((err) => {
					console.log(err);
				});
				break;

			case "blue":
				msg.member.roles.remove(userColourRoles).then(() => {
					addRole("blue");
				}).catch((err) => {
					console.log(err);
				})
				break;

			case "indigo":
				msg.member.roles.remove(userColourRoles).then(() => {
					addRole("indigo");
				}).catch((err) => {
					console.log(err);
				});
				break;

			case "violet":
				msg.member.roles.remove(userColourRoles).then(() => {
					addRole("violet");
				}).catch((err) => {
					console.log(err);
				});
				break;

			case "pink":
				msg.member.roles.remove(userColourRoles).then(() => {
					addRole("pink");
				}).catch((err) => {
					console.log(err);
				})
				break;

			case "garrow":
				msg.member.roles.remove(userColourRoles).then(() => {
					let randInt = Math.floor((Math.random()*8))
					let roleColour = colourArray[randInt];
					addRole(roleColour);
				}).catch((err) => {
					console.log(err);
				});
				break;

			default:
				msg.reply("((Please give a colour of the extended rainbow.))");
		}
    }

    let afterPaint = function() {
    	if (findRole(paintColour) && colourArray.includes(paintColour)) {
			msg.reply(`*The timid maid simply nods before scurrying off, returning moments later with a paint-tin in hand.* "P-please close your eyes..." *she gives little warning before chucking the contents of the tin over you, painting you ${paintColour.toLowerCase()}. The maid tries hard to suppress a smile.`);
		}
    }

	paintUser().then(afterPaint());

}
