module.exports = (msg, settings) => {
    const prefix = settings.get(msg.guild.id).prefix;
    let helpText = `\`${prefix} paint me <color>\` - Takes one of the seven colours of the rainbow or pink, or garrow, and applies a role with that name to the caller. (Setup required)\n`
	  + `\`${prefix} fetch my picture\` - Returns a link to the caller's display picture.\n`
	  + `\`${prefix} flip a coin\` - Flps a coin and outputs the result.\n`
	  + `\`${prefix} roll a <dice definition>\` - Rolls a number of dice. Definitions follow following pattern: \`<number of dice to roll>d<sides of the dice>\`\n`
	  + `\`${prefix} greet\` - Sends a greeting message.\n`
	  + `\`${prefix} make tea\` - Sends a roleplay message of Rin making tea.\n`
	  + `\`${prefix} play <url>\` - Plays either an online audio file, or a youtube video in the voice channel the user is in.\n`
	  + `\`${prefix} transform <@target || me>\` - Prints out a specification for a random form, along with a roleplay message.\n`;

    if (msg.member.hasPermission(`MANAGE_GUILD`) || msg.member.hasPermission(`ADMINISTRATOR`)) {
	helpText += `\n**Guild Manager Only Commands:**\n`
	    + `\`${prefix} set prefix <new prefix>\` - Updates the prefix used for commands.\n`
	    + `\`${prefix} toggle edit notifications\` -  Toggles whether to recieve notifications whenver members edit or delete a message, defaults to off.\n`
	    + `\`${prefix} set notifications channel <channel name>\` - Sets the channel in which notifications for edits and deletes ought to be posted.\n`
	    + `\`${prefix} toggle event logs\` - Toggles whether a log of messages, edits, and deletions ought to be kept, defaults to off.\n`
	    + `\`${prefix} set welcome channel\` - Sets the channel in which welcome messages are sent to new server members.\n`
	    + `\`${prefix} toggle transformation consent\` - Toggles whether Rin's roleplay messages fro the transform command will contain non-consent.\n`;
    }
    
    msg.author.send(`"Uhhh... I think I kept a list of functions somewhere..." *Rin pats down her dress, rummaging through her pockets before finding and presenting a slip of paper:*\n\n${helpText}\n"Here sh-should be a list of all the commands I can run..."`);
}
