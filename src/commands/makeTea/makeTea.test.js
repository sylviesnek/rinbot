import {MockMessage, MockChannel} from 'jest-discordjs-mocks';
const makeTea = require('./makeTea.js');

describe('make tea', () => {
    test('calls message#reply and channel#send', () => {
        const mockMessage = new MockMessage();
        mockMessage.channel = {
            send: () => {}
        };

        spyOn(mockMessage, 'reply').and.stub();
        spyOn(mockMessage.channel, 'send').and.stub();

        makeTea(mockMessage);

        expect(mockMessage.reply).toHaveBeenCalledTimes(1);
        expect(mockMessage.channel.send).toHaveBeenCalledTimes(1);
    });
});