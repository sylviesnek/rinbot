module.exports = (msg) => {
    msg.reply("\"R-right away~\" *scurries off into the kitchen*");
    msg.channel.send("*The click-clacking maid marches proudly into the room, in her hands she carries a tray with a tea set on top.* \"I-I hope this is to your liking...\" *She carefully places the tea set down for everyone to enjoy, as a cog within her makes a whirling sound.*");
}
