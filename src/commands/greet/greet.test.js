import {MockMessage} from "jest-discordjs-mocks";
const greet = require('./greet.js');

describe('greet', () => {
    test('calls message#reply', () => {
        const mockMessage = new MockMessage();
        mockMessage.author = {id: 0};

        spyOn(mockMessage, 'reply').and.stub();

        greet(mockMessage);

        expect(mockMessage.reply).toHaveBeenCalledTimes(1);
    });

    test('gives custom reply to Renee', () => {
        const mockMessage = new MockMessage();
        mockMessage.author = {id: 491461384396079135};

        spyOn(mockMessage, 'reply').and.stub();

        greet(mockMessage);

        expect(mockMessage.reply).toHaveBeenCalledWith(`*Curtsies* "Hello mistress Renée."`);
    });
})