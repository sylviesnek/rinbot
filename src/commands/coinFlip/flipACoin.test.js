import {MockMessage} from 'jest-discordjs-mocks';
const flipACoin = require('./flipACoin.js');

describe('flip a coin', () => {
   test('calls message#reply', () => {
       const mockMessage = new MockMessage();

       spyOn(mockMessage, 'reply').and.stub();

       flipACoin(mockMessage);

       expect(mockMessage.reply).toHaveBeenCalled();
   });
});