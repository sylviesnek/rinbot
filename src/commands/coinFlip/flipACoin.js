module.exports = (msg) => {
    let coinResult = Math.floor(Math.random()*2);

    switch (coinResult) {
        case 0:
            msg.reply(`*Rin deftly slides her right hand over her left, revealing a coin placed atop her thumb - `
            + `which promptly gets flung into the air. After a dramatic catch of the coin above her head, Rin opens `
            + `her hand to reveal the golden coin, displaying a large bushy tail.* "Tails!" *Rin announces with a `
            + `smile, obviously very proud of her theatrics.*`);
            break;

        case 1:
            msg.reply(`*After spending a moment thumbling around, Rin manages to flip a golden coin into the air. `
                + `Nearly tripping over in the process, Rin stumbles forward to catch the coin before revealing it in `
                + `the palm of her hand. The coin itself shows the 'happy' face of a very distracted squirrel* `
                + `"H-heads!" *Rin announces, smiling sheepishly*`);
            break;
    }
}
