module.exports = (msg, settings) => {
    if (msg.member.hasPermission(`MANAGE_GUILD`) || msg.member.hasPermission(`ADMINISTRATOR`)) {
        let guildConfig = settings.get(msg.guild.id);
        guildConfig.logEvents = !guildConfig.logEvents;
        settings.set(msg.guild.id, guildConfig);
        if (guildConfig.logEvents) {
            msg.reply(`"I shall now record all messages, edits and deletions. Please message mistress Renée for a copy of these logs."`);
        }
        else {
            msg.reply(`"I will no longer record all messages, edits and deletions."`);
        }
    }
    else {
        msg.reply(`*The robotic maid avoids eye contact as she shuffles her feet together.* "I-I'm sorry, but you don't have the authority t-to command me to do that..."`);
    }
}