module.exports = (msg, settings) => {
    if (msg.member.hasPermission(`MANAGE_GUILD`) || msg.member.hasPermission(`ADMINISTRATOR`)) {
        let guildConfig = settings.get(msg.guild.id);
        const oldPrefix = guildConfig.prefix;
        const commandString = " set prefix ";
        const newPrefix = msg.content.substr(oldPrefix.length + commandString.length);
        guildConfig.prefix = newPrefix
        settings.set(msg.guild.id, guildConfig);

        msg.reply(`"I have updated the prefix for my commands to ${newPrefix}."`)
    }
    else {
        msg.reply(`*The robotic maid avoids eye contact as she shuffles her feet together.* "I-I'm sorry, but you don't have the authority t-to command me to do that..."`);
    }
}