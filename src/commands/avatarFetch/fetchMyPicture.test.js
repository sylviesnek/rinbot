import {MockMessage} from 'jest-discordjs-mocks';
const fetchMyPicture = require('./fetchMyPicture.js');

describe('fetch my picture', () => {
    test('calls message#reply', () => {
        const mockMessage = new MockMessage();
        mockMessage.author = {displayAvatarURL: () => {return "https://example.com"}};

        spyOn(mockMessage, 'reply').and.stub();

        fetchMyPicture(mockMessage);

        expect(mockMessage.reply).toHaveBeenCalledTimes(1);
    })
});