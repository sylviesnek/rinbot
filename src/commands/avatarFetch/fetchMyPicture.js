module.exports = (msg) => {
    const imageUrl = msg.author.displayAvatarURL();
    msg.reply("Umm, sorry to bother you... Here is your picture: <" + imageUrl + ">");
}
