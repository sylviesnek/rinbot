import {MockMessage} from 'jest-discordjs-mocks';
const roll = require('./roll.js');

describe('roll', () => {
   test('given valid dice calls message#reply', () => {
       const mockMessage = new MockMessage();
       mockMessage.content = "!rin roll 4d6";

       spyOn(mockMessage, 'reply').and.stub();

       roll(mockMessage);

       expect(mockMessage.reply).toHaveBeenCalledTimes(1);
   });

   test('given invalid dice calls message#reply', () => {
       const mockMessage = new MockMessage();
       mockMessage.content = "!rin roll fkldkfj";

       spyOn(mockMessage, 'reply').and.stub();

       roll(mockMessage);

       expect(mockMessage.reply).toHaveBeenCalledTimes(1);
   });

   test('given fractional dice calls message#reply', () => {
       const mockMessage = new MockMessage();
       mockMessage.content = "!rin roll 2.8d8.8";

       spyOn(mockMessage, 'reply').and.stub();

       roll(mockMessage);

       expect(mockMessage.reply).toHaveBeenCalledTimes(1);
   });
});