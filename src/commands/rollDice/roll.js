module.exports = (msg, settings) => {
    const prefix = settings.get(msg.guild.id).prefix;
    const rollCommand = " roll ";

    const rollInput = msg.content.substr(prefix.length + rollCommand.length).toLowerCase();
    const diceInput = rollInput.split('d');
    const dice = [Number(diceInput[0]), Number(diceInput[1])];

    if (diceInput.length != 2) {
        msg.reply(`*Rin looks unsure of herself* "I-I don't think that's a real die..."`);
    }
    else if (!Number.isInteger(dice[0]) || !Number.isInteger(dice[1])) {
        console.log(`Failed to roll ${dice[0]} die/dice ${dice[1]} times`);
        msg.reply(`*Rin tilts her head in confusion* I'm only built to handle integers..."`);
    }
    else {
        let sumRoll = 0;
        for (let diceCount = 0; diceCount < dice[0]; diceCount++) {
            let diceRoll = Math.floor(Math.random() * (dice[1] + 1));
            sumRoll += diceRoll;
        }

        msg.reply(`*Rin spends a moment gathering a set of crystals into a cup, only to shake the cup and fling the crystals`
            + ` across the table, witnessing them morph into various die.* "Looks like you rolled a... ${sumRoll}"`);
    }
}