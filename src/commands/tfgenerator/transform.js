module.exports = (msg, settings) => {
    const heights = ["Giant", "Extremely tall", "Tall", "Above Average", "Average", "Below Average", "Short", "Very Short", "Tiny", "Doll-Sized"];
    const proportions = ["Extremely Curvy", "Very Curvy", "Curvy", "Slightly Curvy", "Flat"];
    const commonSpecies = ["Cow", "Fox", "Cat", "Dog", "Rabbit", "Wolf", "Squirrel", "Ferret", "Deer", "Mouse", "Skunk", "Racoon"];
    const rareSpecies = ["Leopard", "Tiger", "Lion", "Lemur", "Panda"];
    const mythicSpecies = ["Jackalope", "Cabbit", "Eastern Dragon", "Western Dragon", "Hellhound"];
    const modifiers = ["Angelic", "Demonic", "(Fallen) Angelic"];
    const kemonomimiOnlyRareSpecies = ["Butterfly", "Moth", "Merperson", "Bat", "Naga"];
    const commonMaterials = ["Rubber/Latex", "Plush", "Goo", "Inflatable", "Chocolate"];
    const rareMaterials = [ "Gold", "Ruby", "Emerald", "Sapphire", "Crystal", "Marble"];
    const types = {
	human: "Human",
	kemonomimi: "Kemonomimi",
	anthro: "Anthro"
    };

    const replyStartMessages = [
	`*Tilting her head in a dumb dazed expression, the mechanisms in Rin's head make an audible whirling sound. Moments later a small pink pill materialises in the robot's hand. Along with a reciept-like note reading:*\n`,
	`*Rin closes her eyes and emits a ticking noise as she processes the request. Afer a few seconds, the rebotic maid clicks her figers with her left hand, causing a pink pill to materialise in her palm which she holds outwards. Holding out her other palm, a holographic message forms above it:*\n`,
	`*Rin's eyes gloss over as she begins sounding like a microwave. 'Ting!' Opening a compartment in her left arm, Rin presents a small pink pill alongside a formal-looking note reading:*\n`,
	`*Rin holds out her left palm, and stares at it while a pink pill materialises. Once the pill was solid, the robotic maid begins making a series of printer noises. Holding her mouth ajar a note starts printing out between Rin's lips:\n`
    ];
    const replyEndMessages = [
	`"P-please eat th-that pill..." *The maid laughs sheepishly.*`,
	`"E-eating this sh-should transform you..." *The maid gulps awhile making eye contact.*`,
	`"May I-I suggest y-you try eating this..?"`
    ];

    const targetStartMessages = [
	`*A nervous squeak can be heard behind you, before suddenly Rin grabs your waist from behind and jabs a needle of pink liquid into your thigh. Looking down would reveal that the label of the needle reads:*\n`
    ];
    const targetEndMessages = [
	`*Sniffling slightly the robot maid stammers awhile speaking.* "I-I hope one d-day you'll forgive me..."`
    ];

    let getAverageRoll = function(inputArray, rollTimes) {
	let roll = 0;
	for (let i = 0; i < rollTimes; i++) {
	    roll = roll + Math.floor(Math.random() * inputArray.length);
	}
	roll = Math.round(roll / rollTimes);
	return roll;
    };

    let getRandomArrayEntry = function(inputArray) {
	return inputArray[Math.floor(Math.random() * inputArray.length)];
    };

    let getType = function() {
	const typeRoll = Math.random();
	if (typeRoll <= 0.30) {
	    return types.anthro;
	}
	else if (typeRoll <= 0.95) {
	    return types.kemonomimi;
	}
	
	return types.human;
    };
    
    let getSpecies = function(type) {
	const rarityRoll = Math.random();
	if (rarityRoll <= 0.15) {
	    return getRandomArrayEntry(mythicSpecies);
	}
	else if (type === types.kemonomimi && Math.random() <= 0.10) {
	    return getRandomArrayEntry(kemonomimiOnlyRareSpecies);
	}
	else if (rarityRoll <= 0.35) {
	    return getRandomArrayEntry(rareSpecies);
	}

	return getRandomArrayEntry(commonSpecies);
    };

    let getModifier = function() {
	if (Math.random() <= 0.10) {
	    return getRandomArrayEntry(modifiers);
	}

	return null;
    };

    let getMaterial = function() {
	if (Math.random() <= 0.25) {
	    const rarityRoll = Math.random();
	    if (rarityRoll <= 0.30) {
		return getRandomArrayEntry(rareMaterials);
	    }
	    else {
		return getRandomArrayEntry(commonMaterials);
	    }
	}

	return null;
    };

    let getFormDescription = function() {
	const height = heights[getAverageRoll(heights, 2)];
	const proportion = getRandomArrayEntry(proportions);
	const type = getType();
	const species = type != "Human" ? getSpecies(type) : null;
	const modifier = getModifier();
	const material = getMaterial();
	
	let formDescription = `\`\`\`Type: ${type}\nHeight: ${height}\nShape: ${proportion}\n`;
	if (species != null) {
	    formDescription += `Species: ${species}\n`;
	}
	if (modifier != null) {
	    formDescription += `Modifier: ${modifier}\n`;
	}
	if (material != null) {
	    formDescription += `Material: ${material}`;
	}
	formDescription += `\`\`\`\n`

	return formDescription;
    };

    const prefix = settings.get(msg.guild.id).prefix;
    const commandString = " transform "
    const commandParameters = msg.content.substr(prefix.length + commandString.length);
    const formDescription = getFormDescription();
    const transformationConsent = settings.get(msg.guild.id).transformationConsent;
    
    let outputMessageStart = getRandomArrayEntry(replyStartMessages);
    let outputMessageEnd = getRandomArrayEntry(replyEndMessages);
    
    if (!transformationConsent && (transformationConsent != undefined)) {
	outputMessageStart = getRandomArrayEntry(targetStartMessages);
	outputMessageEnd = getRandomArrayEntry(targetEndMessages);
    }
    if (commandParameters.startsWith('me')) {
	const outputReplyMessage = outputMessageStart+ formDescription + outputMessageEnd;
	msg.reply(outputReplyMessage);
    }
    else if (msg.mentions.members.size === 1) {
	const target = msg.mentions.members.first();
	const outputTargetmessage = `<@!${target.id}> ` + outputMessageStart + formDescription + outputMessageEnd;
	msg.channel.send(outputTargetmessage);
    }
    else {
	msg.reply(`"S-sorry... Transform who...?"`);
    }
}
