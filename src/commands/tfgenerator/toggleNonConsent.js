module.exports = (msg, settings) => {
    if (msg.member.hasPermission(`MANAGE_GUILD`) || msg.member.hasPermission(`ADMINISTRATOR`)) {
	let guildConfig = settings.get(msg.guild.id);
	guildConfig.transformationConsent = (guildConfig.transformationConsent === undefined) ? false : !guildConfig.transformationConsent;
	settings.set(msg.guild.id, guildConfig);

	if (guildConfig.transformationConsent) {
	    msg.reply(`"I-I shall now only t-transform people with consent..."`);
	}
	else {
	    msg.reply(`"I'll no longer g-give people an option wh-when it comes to transformations..." *The mechanical maid looks a little nervous.*`);
	}
    }
    else {
        msg.reply(`*The robotic maid avoids eye contact as she shuffles her feet together.* "I-I'm sorry, but you don't have the authority t-to command me to do that..."`);
    }
}
