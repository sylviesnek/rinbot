module.exports = (msg, settings) => {
    if (msg.member.hasPermission(`MANAGE_GUILD`) || msg.member.hasPermission(`ADMINISTRATOR`)) {
        let guildConfig = settings.get(msg.guild.id);
        const prefix = guildConfig.prefix;
        const commandString = " set notifications channel ";
        const newNotificationsChannelName = msg.content.substr(prefix.length + commandString.length).toLowerCase();
        const newNotificationsChannel = msg.member.guild.channels.cache.find(chan => chan.name == newNotificationsChannelName);
        if (newNotificationsChannel) {
            guildConfig.notificationChannel = newNotificationsChannelName;
            settings.set(msg.guild.id, guildConfig);
            msg.reply(`"I have now set the channel for message edit/delete notifications to #${guildConfig.notificationChannel}."`);
        }
        else {
            msg.reply(`*Rin grumbles to herself, pouting her lips somewhat* "I can't find a channel with the name '${newNotificationsChannelName}'.`)
        }
    }
    else {
        msg.reply(`*The robotic maid avoids eye contact as she shuffles her feet together.* "I-I'm sorry, but you don't have the authority t-to command me to do that..."`);
    }
}