module.exports = (msg, settings) => {
    if (msg.member.hasPermission(`MANAGE_GUILD`) || msg.member.hasPermission(`ADMINISTRATOR`)) {
        let guildConfig = settings.get(msg.guild.id);
        guildConfig.reportChanges = !guildConfig.reportChanges;
        settings.set(msg.guild.id, guildConfig);
        if (guildConfig.reportChanges) {
            msg.reply(`"I shall now report in #${guildConfig.notificationChannel} when someone edits or deletes a message."`);
        }
        else {
            msg.reply(`"I shall no longer report when someone edits or deletes a message."`);
        }
    }
    else {
        msg.reply(`*The robotic maid avoids eye contact as she shuffles her feet together.* "I-I'm sorry, but you don't have the authority t-to command me to do that..."`);
    }
}