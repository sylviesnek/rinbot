module.exports = (client,msg) => {
	const loggingService = require("../services/loggingService.js");

    if (msg.guild != undefined) {
	const messageData = `${msg.createdAt} - ${msg.channel.name} - [DELETE] ${msg.author.username}:\n${msg.content}\n\n`;
	const guildId = msg.guild.id;
	const guildConfig = client.settings.get(guildId);

	if (guildConfig.logEvents) {
            loggingService.logEvent(messageData, guildId);
	}
	if (guildConfig.reportChanges) {
            loggingService.sendNotice(client ,messageData, "delete", guildId);
	}
    }
}
