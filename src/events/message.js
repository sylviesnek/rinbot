module.exports = (client,msg) => {
    const loggingService = require("../services/loggingService.js");
    const commandService = require("../services/commandService.js");

    if(msg.author.bot || !msg.guild) {
	return;
    }

    const guildId = msg.guild.id;
    let guildConfig = client.settings.get(guildId);

    if (guildConfig.logEvents) {
	const messageData = `${msg.createdAt} - ${msg.channel.name} - [NEW] ${msg.author.username}:\n${msg.content}\n\n`;
	loggingService.logEvent(messageData, guildId);
    }

    const morningMessages = [
    	`"Good morning everyone. Shall I make breakfast?"`,
	`*Curtsies* "Good morning, perhaps you'd like me to fetch your slippers?"`,
	`"Good morning sleepy heads~"`,
	`"Hello everyone, good morning. Please let me know if there's anything I can do for you."`,
	`"Good morning everyone, I hope you all have a pleasant day."`,
	`"Good morning everyone, please don't forget to brush your teeth."`,
	`*Tilts her head and smiles warmly* "Good morning~ I hope you all have a wonderful day."`,
	`*Rushes into the room carrying a fishing rod and a bucket* "Morning~ Would you like to go fishing today?"`,
	`*Pretends to yawn* "Good morning~"`,
	`*Curtsies* "Good morning~" *The robotic maid pauses in thought* "Mornings always make me remember my time as a toaster"`,
	`"Morning~ The day is Thursday..." *She tilts her head and blinks a few times* "Or is it?"`,
	`"Good morning everyone, shall I feed the cats?" *She laughs sheepishly* "Do we have cats?"`
    ];
    
    function checkMornCooldown() {
	const timer = new Date();
	const currentTime = timer.getTime();
	const morningCooldown = 32400000;
	const lastMorning = guildConfig.lastMorning;
	const appropriateTime = (lastMorning != undefined) ? lastMorning + morningCooldown : 0;
	if (currentTime < appropriateTime){
	    return false;
	}
	guildConfig.lastMorning = currentTime;
	client.settings.set(guildId, guildConfig);
	return true;
    };

    const morningReg = /^(morning|good morning)/i;
    const isMorning = (msg.content.search(morningReg)>-1);
    if (isMorning && checkMornCooldown()) {
	const randMorn = Math.floor((Math.random() * morningMessages.length));
	msg.channel.send(morningMessages[randMorn]);
    }

    if (msg.content.indexOf(guildConfig.prefix) === 0) {
	const commandString = msg.content.substr(guildConfig.prefix.length + 1).toLowerCase();
	try {
	    commandService.runCommand(commandString, msg, client.settings);
	}
	catch (exception) {
	    msg.reply(`"S-sorry! S-something's gone wrong..." *Rin holds up her right wrist, revealing many loose wires sticking out.* "I-I'll inform Renée."`);
	    const reneeUser = client.users.cache.find((user) => {
		return user.id === '491461384396079135';
	    });
	    reneeUser.send(`"H-hi... I hit an error:\n" + \`\`\` + ${exception}\`\`\``);
	}
    }
};
