module.exports = (client, member) => {
    const guildConfig = client.settings.get(member.guild.id);
    let msgChannel = member.guild.channels.cache.find(channel => channel.name == guildConfig.welcomeChannel
        && channel.guild.id === member.guild.id);
    if (!msgChannel){
	    return;
    }
    msgChannel.send(`*Curtsies* "Welcome ${member} to the Discord server. Please make sure you read the rules."`);
};
