module.exports = (client, msgOld, msgNew) => {
    const loggingService = require("../services/loggingService.js");

    if (msgOld.guild != undefined) {
	const messageData = `${msgNew.createdAt} - ${msgOld.channel.name} - [EDIT] ${msgOld.author.username}:\n${msgOld.content}\nBECAME:\n${msgNew.content}\n\n`;
	const guildId = msgNew.guild.id;
	const guildConfig = client.settings.get(guildId);

	if (guildConfig.logEvents) {
            loggingService.logEvent(messageData, guildId);
	}
	if (guildConfig.reportChanges) {
            loggingService.sendNotice(client, messageData, "edit", guildId);
	}
    }
}
