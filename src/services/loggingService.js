const fs = require('fs');

let logEvent = function(eventData, guildId) {
    if (guildId != undefined)
    {
	fs.appendFile(`../eventLog${guildId}.txt`, eventData, 'utf8', (err) => {
            if (err) {
		console.log(`Failure to save message log.`);
		throw err;
            }
	});
    }
}

let sendNotice = function(client, eventData, noticeType, guildId) {
    if (guildId != undefined)
    {
	const notificationChannelName = client.settings.get(guildId).notificationChannel;
	const notificationChannel = client.channels.cache.find(channel => channel.name === notificationChannelName	&& channel.guild.id === guildId);
	if (notificationChannel) {
            console.log(`${notificationChannel.name} - sending ` + noticeType + ` notice.`)
            notificationChannel.send(eventData);
	}
    }
}

module.exports = {logEvent, sendNotice};
