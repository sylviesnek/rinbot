const fs = require('fs');

function createCommand(commandTrigger, commandFile, sendSettings = false) {
    return {
        trigger: commandTrigger,
        commandFile: commandFile,
        sendSettings: sendSettings
    }
}

const commandList = [
    createCommand(`paint me`, `../commands/applyPaint/paintMe.js`, true),
    createCommand(`fetch my picture`, `../commands/avatarFetch/fetchMyPicture.js`),
    createCommand(`greet`, `../commands/greet/greet.js`),
    createCommand(`make tea`, `../commands/makeTea/makeTea.js`),
    createCommand(`play`, `../commands/musicPlayer/play.js`, true),
    createCommand(`roll`, `../commands/rollDice/roll.js`, true),
    createCommand(`flip a coin`, `../commands/coinFlip/flipACoin.js`),
    createCommand(`set prefix`, `../commands/setPrefix/setPrefix.js`, true),
    createCommand(`toggle edit notifications`, `../commands/notifications/toggleEditNotifications.js`, true),
    createCommand(`set notifications channel`, `../commands/notifications/setNotificationsChannel.js`, true),
    createCommand(`toggle event logs`, `../commands/eventLog/toggleEventLogging.js`, true),
    createCommand(`set welcome channel`, `../commands/setWelcomeChannel/setWelcomeChannel.js`, true),
    createCommand(`transform`, `../commands/tfgenerator/transform.js`, true),
    createCommand(`toggle transformation consent`, `../commands/tfgenerator/toggleNonConsent.js`, true),
    createCommand(`help`, `../commands/help/help.js`, true)
]

let runCommand = function(commandTrigger, message, settings) {
    const commandToRun = commandList.find(command => commandTrigger.startsWith(command.trigger));
    if (commandToRun !== undefined) {
        const commandCode = require(commandToRun.commandFile);
        commandToRun.sendSettings ? commandCode(message, settings) : commandCode(message);
    }
    else{
        message.reply(`"I-I'm sorry... I'm not sure I understand what you mean..."`);
    }
}

module.exports = {runCommand};
