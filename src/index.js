const Discord = require('discord.js');
const fs = require("fs");
const Config = require('../config.json');
const client = new Discord.Client();
const Enmap = require('enmap');

client.on('ready', () => {
    client.lastMorn = 0;
    client.user.setActivity(Config.activity);

    console.log(`Logged in as ${client.user.tag}!`);
    console.log("Maid online!");
});

client.settings = new Enmap({
	name: "settings",
	fetchAll: false,
	autoFetch: true,
	cloneLevel: "deep",
	autoEnsure: {
	    prefix: "!rin",
	    logEvents: false,
	    reportChanges: false,
	    transformationConsent: true,
	    welcomeChannel: "meeting-hall",
	    notificationChannel: "bot-notice"
	}
});

let setupEvents = function(error,files) {
    if (error){
	return console.error(error);
    }
    files.forEach(file => {
	if (!file.endsWith(".js")) {
	    console.log("Ingoring file: " + file);
	    return;

	}
	const eventFile = require("./events/" + file);
	let eventName = file.split(".")[0];
	client.on(eventName,eventFile.bind(null,client));
	//Clean up stuff that I don't understand, see http://anidiots.guide/first-bot/a-basic-command-handler
	delete require.cache[require.resolve("./events/"+file)];
	});
}

fs.readdir("./events",setupEvents);

let leaveVoiceChannelsIfEmpty = function() {
    client.guilds.cache.each(guild => {
	const voiceChannel = guild.me.voice.channel;
	if (voiceChannel != null){
	    const members = Array.from(voiceChannel.members.values());
	    if (members.length == 1){
		voiceChannel.leave();
	    }
	}
    });
}

setInterval(leaveVoiceChannelsIfEmpty, 20000);

client.login(Config.token);
